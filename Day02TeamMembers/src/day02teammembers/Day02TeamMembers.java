package day02teammembers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.*;

public class Day02TeamMembers {

    static Map< String, ArrayList<String>> teams = new HashMap<>();
    static final String FILE_NAME = "teams.txt";
    static Scanner input = new Scanner(System.in);
    static Set<String> person = new HashSet<>();

    public static void loadMapFromFile() {
        // read from file
        try (Scanner fileIinput = new Scanner(new File(FILE_NAME))) { // IOException
            while (fileIinput.hasNextLine()) {  // IllegalStateException
                String line = fileIinput.nextLine();
                // first split to get team name
                String[] team = line.split(":", 2); // PatternSyntaxException
                // second split to get names of team memebers
                String[] name = team[1].split(",");

                // create ArrayList of names and add them to HashSet person
                ArrayList<String> memb = new ArrayList<>();
                for (String s : name) {
                    memb.add(s);
                    person.add(s);
                }

                // create HashMap
                teams.put(team[0], memb);
            }
        } catch (IOException ex) {
            System.out.println("Error: file not found. " + ex.getMessage());
        } catch (IllegalStateException ex) {
            System.out.println("Error: " + ex.getMessage());
        } catch (PatternSyntaxException ex) {
            System.out.println("Error: " + ex.getMessage());
        } 
    }

    public static void displayTeamsPerPerson() {
        try {
            for (String p : person) { // iterate through HashSet person - NullPointerException
                System.out.print(p + " plays in: ");
                for (Map.Entry<String, ArrayList<String>> entry : teams.entrySet()) { // iterate through HashMap teams
                    ArrayList<String> al = entry.getValue();
                    for (String s : al) { // iterate through names from the ArrayList values in the HashMap teams
                        if (p.equals(s)) {
                            System.out.print(entry.getKey() + ", "); // display keys for values 
                        }
                    }
                }
                System.out.println("");
            }
        } catch (NullPointerException ex) {
            System.out.println("Error: list doesnot exist. " + ex.getMessage());
        }

    }

    public static void main(String[] args) {
        loadMapFromFile();
        displayTeamsPerPerson();
    }

}
