package day01stackinterpreter;

import java.util.*;
import java.io.*;

// FileNameFilter implementation - to filter file extensions
class MyFileNameFilter implements FilenameFilter {

    private String extension;

    public MyFileNameFilter(String extension) {
        this.extension = extension.toLowerCase();
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(extension);
    }

}

public class Day01StackInterpreter {

    // Decalre class variables
    static final String DIRECTORY = "C:/Users/garde/Documents/JAVA/java3/Day01StackInterpreter";
    static final String EXTENSION = ".txt";
    static Scanner input = new Scanner(System.in);
    static String[] fileNames;
    static Stack<Double> stack = new Stack<>();

    // To display all txt files in the directory
    static void displayFiles() {
        System.out.println("Choose the file: ");
        File filePath = new File(DIRECTORY);
        if (!filePath.exists()) {
            System.out.println("Directory doesn't exists");
        }
        fileNames = filePath.list(new MyFileNameFilter(EXTENSION));
        for (String name : fileNames) {
            System.out.println(name);
        }

    }

    // User choice of the file from the stack
    static String fileChoice() {
        System.out.print("Enter the name of the file: ");
        String str = input.nextLine();
        boolean check = false;
        for (String n : fileNames) {
            if (n.contains(str)) {
                check = true;
                break;
            }
        }
        return check ? str : "Invalid file choice";
    }

    // read data from user and push to stack
    public static void myRead(String str) {
        double num1 = 0;
        try {
            System.out.print(str);
            num1 = input.nextDouble();
        } catch (InputMismatchException ex) {
            System.out.println("Error: wrong input." + ex.getMessage());
            input.next();
        }
        stack.push(num1);
    }

    // push values to stack
    public static void myPush(String str) {
        try {
            Double num = Double.parseDouble(str);
            stack.push(num);
        } catch (NumberFormatException ex) {
            System.out.println("Error: wrong number." + ex.getMessage());
        }
        //    System.out.println(stack.peek()); -- to test push
    }

    // pop value from stack
    public static void myPop() {
        try {
            stack.pop();
        } catch (NullPointerException ex) {
            System.out.println("Error: stack is empty. " + ex.getMessage());
        }
    }

    // add 2 values from the stack
    public static void myAdd() {
        try {
            double result = stack.pop() + stack.pop();
            stack.push(result);
        } catch (NullPointerException ex) {
            System.out.println("Error: stack is empty. " + ex.getMessage());
        }
    }

    // substract 2 values from the stack
    public static void mySubstract() {
        try {
            double result = stack.pop() - stack.pop();
            stack.push(result);
        } catch (NullPointerException ex) {
            System.out.println("Error: stack is empty. " + ex.getMessage());
        }
    }

    // mupltiply 2 values from the stock
    public static void myMultiply() {
        try {
            double result = stack.pop() * stack.pop();
            stack.push(result);
        } catch (NullPointerException ex) {
            System.out.println("Error: stack is empty. " + ex.getMessage());
        }
    }

    // divide 2 values from the stock
    public static void myDivide() {
        try {
            double result = stack.pop() / stack.pop();
            stack.push(result);
        } catch (ArithmeticException ex) {
            System.out.println("Error: division by zero. " + ex.getMessage());
        }catch (EmptyStackException ex) {
            System.out.println("Error: stack is empty. " + ex.getMessage());
        }
    }

    // exchange order of 2 values from the stock
    public static void myExchange() {
        try {
            double num1 = stack.pop();
            double num2 = stack.pop();
            stack.push(num1);
            stack.push(num2);
        } catch (EmptyStackException ex) {
            System.out.println("Error: stack is empty. " + ex.getMessage());
        }
    }

    // print the top value from the stack
    public static void myPrint(String str) {
        if (stack.size() > 0) {
            System.out.printf("%s %.2f \n", str, stack.peek());
        } else {
            System.out.println("Nothing to print");
        }
    }

    public static void main(String[] args) {

        // display all files in directory
        
        displayFiles();

        // user's choice of file
        String file = fileChoice();
        System.out.println("File: " + file);

        // run the file 
        String message = "";
        try {
            try (Scanner inputFile = new Scanner(new File(file))) {
                while (inputFile.hasNextLine()) {
                    String[] command = inputFile.nextLine().split(":");
//                    // check for : in the string and split it in 2
//                    if (command.contains(":")) {
//                        String[] strArray = command.split(":", 2);
//                        command = strArray[0];
//                        message = strArray[1];
//                    }
                    switch (command[0]) {
                        case "read":
                            myRead(command[1]);
                            break;
                        case "push":
                            myPush(command[1]);
                            break;
                        case "pop":
                            myPop();
                            break;
                        case "print":
                            myPrint(command[1]);
                            break;
                        case "add":
                            myAdd();
                            break;
                        case "sub":
                            mySubstract();
                            break;
                        case "mul":
                            myMultiply();
                            break;
                        case "div":
                            myDivide();
                            break;
                        case "exchange":
                            myExchange();
                            break;
                        default:
                            System.out.println("Error: Wrong command line");
                                 }
                }
            }
        } catch (IOException ex) {
            System.out.println("Eroor:" + ex.getMessage());
        } catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("Eroor:" + ex.getMessage());
        }
    }
}
