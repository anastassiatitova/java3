package day01random;

import java.util.*;

public class Day01Random {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int count = 0;
        int min = 0;
        int max = 0;

        try {
            System.out.print("How many random numbers do you want to generate:");
            count = input.nextInt();
            if (count < 0) {
                System.out.print("Error: negative number");
                System.exit(0);
            }
            System.out.print("Enter minimum: ");
            min = input.nextInt();
            if (min < 0) {
                System.out.println("Error: negative number");
                System.exit(0);
            }
            System.out.print("Enter maximum: ");
            max = input.nextInt();
            if (max < 0) {
                System.out.println("Error: negative number");
                System.exit(0);
            }

            if (max < min) {
                System.out.println("Error: max in smaller than min");
                System.exit(0);
            }
        } catch (InputMismatchException ex) {
            System.out.println("Incorrect input: an integer is required");
        }
        for (int i = 0; i < count; i++) {
            int val = (int)(Math.random() * (max - min + 1))+ min;
            System.out.printf("%s%d", (i == 0 ? "": ", "), val);
        }

    }

}
