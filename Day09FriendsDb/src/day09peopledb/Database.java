package day09peopledb;

import java.sql.*;

import java.util.ArrayList;

public class Database {

    private static String dbUrl = "jdbc:mysql://localhost:3306/day08people";
    private static String username = "root";
    private static String password = "Sdf93jka63>";

    private Connection conn = null; // make connection private

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbUrl, username, password);
    }

    public ArrayList<Person> getAllPeople() throws SQLException {
        ArrayList<Person> list = new ArrayList<>();
        String sql = "SELECT * FROM people";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a godd practice to use try-with-resources for ResultSet so it is frred up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String name = result.getString("name");
                int age = result.getInt("age");
//                System.out.printf("%d : %s is %d y/o \n", id, name, age);
                list.add(new Person(id, name, age));
            }
        }
        return list;
    }

    public void addPerson(Person person) throws SQLException {
        String sql = "INSERT INTO people VALUES (NULL,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, person.name);
        statement.setInt(2, person.age);
        statement.executeUpdate();
        System.out.println("Record inserted");
    }

    public void updatePerson(Person person) throws SQLException {
        String sql = "UPDATE people SET name=?, age=? WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, person.name);
        statement.setInt(2, person.age);
        statement.setInt(3, person.id);
        statement.executeUpdate();
        System.out.println("Record updated: " + person.id);
    }

    public void deletePerson(Person person) throws SQLException {
        String sql = "DELETE FROM people WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, person.id);
        statement.executeUpdate();
        System.out.println("Record deleted id =" + person.id);
    }

}
