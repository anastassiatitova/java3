package day08firstdb;

import java.sql.*;
import java.util.Random;

public class Day08FirstDb {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String dbUrl = "jdbc:mysql://localhost:3306/day08people";
        String username = "root";
        String password = "Sdf93jka63>";
        Connection conn = null;
        Random random = new Random();

        try {
            conn = DriverManager.getConnection(dbUrl, username, password);

            if (conn != null) {
                System.out.println("Connected");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        
        // insert into database
 /*       try {
            String sql = "INSERT INTO people VALUES (NULL,?,?)";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "Mary");
            statement.setInt(2, 45);
            statement.executeUpdate();
            System.out.println("Record updated");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
      
        // select from databse
        try {
            String sql = "SELECT * FROM people";
            PreparedStatement statement = conn.prepareStatement(sql);
            // it is a godd practice to use try-with-resources for ResultSet so it is frred up as soon as possible
            try (ResultSet result = statement.executeQuery(sql)) {
                while (result.next()) { // has next row to read
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    System.out.printf("%d : %s is %d y/o \n", id, name, age);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
*/       

        // Update
        try {
            int id = 2;
            int newAge = 23;
            String sql = "UPDATE people SET age=? WHERE id=? ";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, newAge);
            statement.setInt(2,id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
 /*      try {
            int id = 1;
            String sql = "DELETE FROM people WHERE id=? ";
            PreparedStatement statement = conn.prepareStatement(sql);
           statement.setInt(1,id);
            statement.executeUpdate();
            System.out.println("Record deleted id =" + id);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } */
        
    }

}
