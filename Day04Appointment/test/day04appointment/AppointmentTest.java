/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day04appointment;

import day04appointment.Appointment.Reason;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author garde
 */
public class AppointmentTest {

//    public AppointmentTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        Appointment instance = null;
        LocalDateTime expResult = null;
        LocalDateTime result = instance.getDate();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testSetDate() throws Exception {
        System.out.println("setDate");
        LocalDateTime date = null;
        Appointment instance = null;
        instance.setDate(date);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetDurationMinutes() {
        System.out.println("getDurationMinutes");
        Appointment instance = null;
        int expResult = 0;
        int result = instance.getDurationMinutes();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testSetDurationMinutes() throws Exception {
        System.out.println("setDurationMinutes");
        int durationMinutes = 0;
        Appointment instance = null;
        instance.setDurationMinutes(durationMinutes);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetName() throws DataInvalidException {
        System.out.println("getName");
        HashSet<Reason> reasonList = new HashSet<Reason>();
        reasonList.add(Reason.TESTS);
        LocalDateTime dt = LocalDateTime.of(2021, Month.MARCH, 15, 10, 20);
        Appointment instance = new Appointment(dt, 20, "A B", reasonList);
        String expResult = "A B";
        String result = instance.getName();
        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
    }

    @Test
    public void testSetName() throws Exception {
        System.out.println("setName");
        String name = "";
        Appointment instance = null;
        instance.setName(name);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetReasonList() throws DataInvalidException {
        System.out.println("getReasonList");
        HashSet<Reason> reasonList = new HashSet<Reason>();
        reasonList.add(Reason.TESTS);
        LocalDateTime dt = LocalDateTime.of(2021, 03, 12, 10, 20);
        Appointment instance = new Appointment(dt, 20, "A B", reasonList);
        HashSet<Appointment.Reason> expResult = reasonList;
        HashSet<Appointment.Reason> result = instance.getReasonList();
        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
    }

    @Test
    public void testSetReasonList() throws Exception {
        System.out.println("setReasonList");
        HashSet<Reason> reasonList = new HashSet<Reason>();
        reasonList.add(Reason.TESTS);
        LocalDateTime dt = LocalDateTime.of(2021, 03, 12, 10, 20);
        Appointment instance = new Appointment(dt, 20, "A B", reasonList);
        instance.setReasonList(reasonList);
//        fail("The test case is a prototype.");
    }

    @Test
    public void testToString() throws DataInvalidException {
        System.out.println("toString");
        HashSet<Reason> reasonList = new HashSet<Reason>();
        reasonList.add(Reason.TESTS);
//        reasonList.add(Reason.REFERRAL);
        LocalDateTime dt = LocalDateTime.of(2021, 03, 12, 10, 20);
        Appointment instance = new Appointment(dt, 20, "A B", reasonList);
        String expResult = "Appointment on 2021-03-12 at 10:20 for 20 minutes for A B, reasons: TESTS";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    @Test
    public void testToDataString() throws DataInvalidException {
        System.out.println("toDataString");
        HashSet<Reason> reasonList = new HashSet<Reason>();
        reasonList.add(Reason.TESTS);
        LocalDateTime dt = LocalDateTime.of(2021, 03, 12, 10, 20);
        Appointment instance = new Appointment(dt, 20, "A B", reasonList);
        String expResult = "2021-03-12T10:20;20;A B;TESTS";
        String result = instance.toDataString();
        assertEquals(expResult, result);
    }

    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Object o = null;
        Appointment instance = null;
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

}
