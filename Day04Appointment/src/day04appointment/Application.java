package day04appointment;

import day04appointment.Appointment.Reason;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Application {

    static final String FILE_NAME = "appointments.txt";
    static Scanner input = new Scanner(System.in);
    static HashSet<Reason> reasonList;
    static ArrayList<Appointment> appList = new ArrayList<>();
    static TreeMap<LocalDateTime, LocalDateTime> availabilityMap = new TreeMap<LocalDateTime, LocalDateTime>();

    public static void readFromFile() {
        try (Scanner inputFile = new Scanner(new File(FILE_NAME))) { // ex IOException
            while (inputFile.hasNextLine()) {
                Appointment app = new Appointment(inputFile.nextLine());
                appList.add(app);
                availabilityMap.put(app.getDate(), app.getDate().plusMinutes(app.getDurationMinutes()));
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        } catch (DataInvalidException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public static void saveDataToFile() {
        try (PrintWriter output = new PrintWriter(new File(FILE_NAME))) { // ex IOException
            for (Appointment td : appList) {
                output.println(td.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public static int menu() {
        System.out.println("");
        System.out.print("Main menu:\n"
                + "1. Make an appointment\n"
                + "2. List appointments by Date\n"
                + "3. List appointments by Name\n"
                + "4. List appointments by their first Reason\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = inputInt(); // user input int 
        return choice; // return user's choice
    }

    public static void makeAppointment() {
        try {
            // input from user
            System.out.print("Enter appointment date (yyyy-mm-dd):");
            LocalDate date = inputDate();
            System.out.print("Enter appointment time (hh:mm): ");
            LocalTime time = inputTime();
            System.out.print("Enter duration (20, 40, or 60): ");
            int duration = inputInt();
            System.out.print("Enter your name: ");
            String name = input.nextLine();
            System.out.print("Possible reasons for visit are Checkup, Referral, Tests, FollowUp, Unwell.\n"
                    + "Enter your reason for visit:");
            String visit = input.nextLine().toUpperCase();

            //convert to strings to LocalDateTime
            LocalDateTime dateTime = LocalDateTime.of(date, time);
            availabilityMap.put(dateTime, dateTime.plusMinutes(duration));

            if (checkAvailabitity(dateTime, duration)) {
                return;
            };
            HashSet<Reason> list = stringToHashSet(visit);

            // create appointment and add to the list
            Appointment app = new Appointment(dateTime, duration, name, list);
            appList.add(app);

            System.out.println("Appointment created.");
            System.out.println(app.toString());

        } catch (DataInvalidException | NullPointerException | IllegalArgumentException | DateTimeException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public static boolean checkAvailabitity(LocalDateTime dateTime, int duration) {
        LocalDateTime endOfApp = dateTime.plusMinutes(duration);
        LocalDateTime appStart1 = availabilityMap.floorKey(dateTime);
        LocalDateTime appEnd1 = availabilityMap.get(appStart1);
        LocalDateTime appStart2 = availabilityMap.ceilingKey(dateTime);
        if (dateTime.isBefore(appEnd1) || endOfApp.isAfter(appStart2)) {
            System.out.printf("This time slot: %s is not available . Please rebook your appointment.\n", appStart1.toString().replace("T", " "));
            return true;
        }
        return false;
//           
//        Set set = availabilityMap.entrySet();
//        Iterator iterator = set.iterator();
//        while (iterator.hasNext()) {
//            Map.Entry mentry = (Map.Entry) iterator.next();
//            System.out.print("key is: " + mentry.getKey().toString() + " & Value is: ");
//            System.out.println(mentry.getValue().toString());
//        
//        }
    }

    static int inputInt() {
        while (true) {
            try {
                int num = input.nextInt();
                input.nextLine();
                return num;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid integer, please try again: ");
                input.next(); // consume invalid input
            }
        }
    }

    static LocalDate inputDate() {
        while (true) {
            try {
                String str = input.nextLine();
                LocalDate date = LocalDate.parse(str);
                return date;
            } catch (IllegalArgumentException | DateTimeException ex) {
                System.out.print("Invalid date, please try again: ");

            }
        }
    }

    static LocalTime inputTime() {
        while (true) {
            try {
                String str = input.nextLine();
                LocalTime time = LocalTime.parse(str);
                return time;
            } catch (IllegalArgumentException | DateTimeException ex) {
                System.out.print("Invalid time, please try again: ");

            }
        }
    }

    public static HashSet<Reason> stringToHashSet(String visit) {
        HashSet<Reason> set = new HashSet<>();
        String[] reasons = visit.split(",");
        for (String s : reasons) {
            s = s.trim().toUpperCase();
            Reason r = Reason.valueOf(s);
            set.add(r);  // ex IllegalArgumentException, NullPointerException
        }
        return set;
    }

    public static void main(String[] args) throws DataInvalidException {
        readFromFile();

        for (Appointment a : appList) {
            System.out.println(a.toString());
        }

        while (true) {
            int choice = menu();
            switch (choice) {
                case 0: { // save data and exit
                    saveDataToFile();
                    System.out.println("Exiting. Good bye!");
                    return;
                }
                case 1: // make a new appointment
                    makeAppointment();
                    break;
                case 2:  // sort all appointments by date
                    Collections.sort(appList, Appointment.dateComparator);
                    for (Appointment a : appList) {
                        System.out.println(a.toString());
                    }
                    break;
                case 3: // sort all appointments by name
                    Collections.sort(appList, Appointment.nameComparator);
                    for (Appointment a : appList) {
                        System.out.println(a.toString());
                    }
                    break;
                case 4: // sort all appointments by reason
                    Collections.sort(appList, Appointment.reasonComparator);
                    for (Appointment a : appList) {
                        System.out.println(a.toString());
                    }
                    break;
                default: // invalid user's choice
                    System.out.println("This is not a valid Menu Option! Please Select Another");
            }
        }
    }

}
