package day04appointment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;

public class Appointment implements Comparable<Object>{

    private LocalDateTime date;
    private int durationMinutes;
    private String name;
    private HashSet<Reason> reasonList = new HashSet<>();

     public enum Reason {
        TESTS,
        CHECKUP,
        REFERRAL,
        FOLLOWUP,
        UNWELL
    };

    public Appointment(LocalDateTime date, int durationMinutes, String name, HashSet<Reason> reasonList) throws DataInvalidException {
        setDate(date);
        setDurationMinutes(durationMinutes);
        setName(name);
        setReasonList(reasonList);
    }

    public Appointment(String dataLine) throws DataInvalidException {
        try {
            // parse string read from file
            String[] str = dataLine.split(";"); // PatternSyntaxException (sub of IllegalArgumentException)
            // conver string to date
            LocalDateTime date = LocalDateTime.parse(str[0]); // ex DateTimeException 
            // convert string to integer
            durationMinutes = Integer.parseInt(str[1]); // ex NumberFormatException
            // convert string to HashSet of enums
            String[] reasons = str[3].split(",");
            for (String s : reasons) {
                s = s.trim().toUpperCase();
                Reason r = Reason.valueOf(s);
                reasonList.add(r);  // ex IllegalArgumentException, NullPointerException
            }
            setDate(date);
            setDurationMinutes(durationMinutes);
            setName(str[2]);
            setReasonList(reasonList);
        } catch (IllegalArgumentException | NullPointerException | DateTimeException ex) {
            throw new DataInvalidException(ex.getMessage());
        }

    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) throws DataInvalidException {
        HashSet<Integer> set = new HashSet<>();
        set.add(20);
        set.add(40);
        set.add(0);
        
//        LocalDate today = LocalDate.now();
//        LocalDate dateWithoutTime = date.toLocalDate();
//        if (dateWithoutTime.isBefore(today) || dateWithoutTime.equals(today) || !set.contains(date.getMinute())) {
//            throw new DataInvalidException("Appointment can only be made for tomorrow or a later date and minutes can be only: 00, 20, 40.");
//        }

          LocalDateTime today = LocalDateTime.now() ;
        LocalDateTime plusHundred = today.plusDays(100);
        if (date.isBefore(today) || date.isAfter(plusHundred)) {
            throw new DataInvalidException("Appointment can only be made for tomorrow or a later date and minutes can be only: 00, 20, 40.");
          
        } 
        this.date = date;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) throws DataInvalidException {
        HashSet<Integer> set = new HashSet<>();
        set.add(20);
        set.add(40);
        set.add(60);

        if (!set.contains((Integer) durationMinutes)) {
            throw new DataInvalidException("Only 20 min, 40 min or 1 hour appointments are allowed. ");
        }
        this.durationMinutes = durationMinutes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DataInvalidException {
        if (!name.matches("^[a-zA-Z0-9\\s\\-\\,\\.\\(\\)\\'\\\"]*$")) {
            throw new DataInvalidException("In the name only allowed: letters, numbers,empty spaces, dashes, commas, dots, parentheses, single and double quotes.");
        }
        this.name = name;
    }

    public HashSet<Reason> getReasonList() {
        return reasonList;
    }

    public void setReasonList(HashSet<Reason> reasonList) throws DataInvalidException {
        if (reasonList.size() == 0) {
            throw new DataInvalidException("Reason list can not be empty.");
        }
        this.reasonList = reasonList;
    }

    @Override
    public String toString() {
        return "Appointment on " + date.toLocalDate().toString() + " at " + date.toLocalTime().toString() + " for " + durationMinutes + " minutes for " + name
                + ", reasons: " + reasonList.toString().replace("[", "").replace("]", "");
    }

    public String toDataString() {
        return date + ";" + durationMinutes + ";" + name + ";" + reasonList.toString().replace("[", "").replace("]", "");
    }

     @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static Comparator<Appointment> dateComparator = new Comparator<Appointment>() {
        @Override
        public int compare(Appointment a1, Appointment a2) {
            return (int)(a1.getDate().compareTo(a2.getDate()));
        }
    };
    
    public static Comparator<Appointment> nameComparator = new Comparator<Appointment>() {
        @Override
        public int compare(Appointment a1, Appointment a2) {
            return (int)(a1.getName().compareTo(a2.getName()));
        }
    };
    
        public static Comparator<Appointment> reasonComparator = new Comparator<Appointment>() {
        @Override
        public int compare(Appointment a1, Appointment a2) {
            return (int)(a1.getReasonList().iterator().next().compareTo(a2.getReasonList().iterator().next()));
        }
    };
    
}
