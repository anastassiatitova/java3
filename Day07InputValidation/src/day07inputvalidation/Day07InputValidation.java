/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07inputvalidation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author garde
 */
public class Day07InputValidation extends javax.swing.JFrame {

    private int height = 170;

    /**
     * Creates new form Day07InputValidation
     */
    public Day07InputValidation() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupGender = new javax.swing.ButtonGroup();
        lblName = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        lblPassport = new javax.swing.JLabel();
        lblContinent = new javax.swing.JLabel();
        lblGender = new javax.swing.JLabel();
        lblPets = new javax.swing.JLabel();
        lblHeight = new javax.swing.JLabel();
        lblHeightDisplay = new javax.swing.JLabel();
        lblBirthdate = new javax.swing.JLabel();
        tfPassport = new javax.swing.JTextField();
        comboContinent = new javax.swing.JComboBox<>();
        rbGenderMale = new javax.swing.JRadioButton();
        rbGenderFemale = new javax.swing.JRadioButton();
        rbGenderOther = new javax.swing.JRadioButton();
        cbPetsFish = new javax.swing.JCheckBox();
        cbPetsPig = new javax.swing.JCheckBox();
        cbPetsOther = new javax.swing.JCheckBox();
        sliderHeight = new javax.swing.JSlider();
        ftfBirthdate = new javax.swing.JFormattedTextField();
        btSubmit = new javax.swing.JButton();
        lblEntry = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        lblName.setText("Name");

        lblPassport.setText("Passport No");

        lblContinent.setText("Continent");

        lblGender.setText("Gender");

        lblPets.setText("Pets");

        lblHeight.setText("Height");

        lblHeightDisplay.setText("170 cm");

        lblBirthdate.setText("Date of Birth");

        comboContinent.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Please choose", "North America", "South America", "Europe", "Asia", "Africa", "Australia" }));

        btnGroupGender.add(rbGenderMale);
        rbGenderMale.setText("Male");

        btnGroupGender.add(rbGenderFemale);
        rbGenderFemale.setText("Female");

        btnGroupGender.add(rbGenderOther);
        rbGenderOther.setText("N/A - Other");

        cbPetsFish.setText("Fish");

        cbPetsPig.setText("Pig");

        cbPetsOther.setText("Other");

        sliderHeight.setMajorTickSpacing(50);
        sliderHeight.setMaximum(250);
        sliderHeight.setMinimum(100);
        sliderHeight.setMinorTickSpacing(50);
        sliderHeight.setPaintLabels(true);
        sliderHeight.setPaintTicks(true);
        sliderHeight.setValue(170);
        sliderHeight.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderHeightStateChanged(evt);
            }
        });

        ftfBirthdate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT))));
        ftfBirthdate.setAutoscrolls(false);

        btSubmit.setText("Verify and Add");
        btSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSubmitActionPerformed(evt);
            }
        });

        lblEntry.setText("...");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblContinent, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblGender, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPets, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblHeight, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(lblHeightDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(lblPassport, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboContinent, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbGenderMale)
                                    .addComponent(cbPetsFish, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbGenderFemale)
                                    .addComponent(cbPetsPig, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbGenderOther)
                                    .addComponent(cbPetsOther, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(btSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ftfBirthdate, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sliderHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfPassport, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(lblBirthdate, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(322, 322, 322))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblEntry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblBirthdate, lblContinent, lblGender, lblHeight, lblName, lblPassport, lblPets});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cbPetsFish, cbPetsOther, cbPetsPig, rbGenderFemale, rbGenderMale, rbGenderOther});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {ftfBirthdate, tfName, tfPassport});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPassport)
                    .addComponent(tfPassport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblContinent, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboContinent, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblGender, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbGenderMale, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(rbGenderFemale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rbGenderOther, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(lblPets)
                        .addComponent(cbPetsFish))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbPetsPig)
                        .addComponent(cbPetsOther)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblHeight)
                    .addComponent(lblHeightDisplay)
                    .addComponent(sliderHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblBirthdate)
                    .addComponent(ftfBirthdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(btSubmit)
                .addGap(18, 18, 18)
                .addComponent(lblEntry)
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblBirthdate, lblContinent, lblGender, lblHeight, lblName, lblPassport, lblPets});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {ftfBirthdate, tfName, tfPassport});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbPetsFish, cbPetsOther, cbPetsPig});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSubmitActionPerformed

        String name = tfName.getText();
        if (!Pattern.matches("^[a-zA-Z\\s\\-]{1,50}$", name)) {
            JOptionPane.showMessageDialog(this,
                    "Name must be 1-50 characters long, only letters, space and dash are allowed.",
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
            tfName.setText("");
            return;
        }
        String passport = tfPassport.getText().toUpperCase();
        if (!Pattern.matches("[A-Z]{2}[0-9]{6}", passport)) {
            JOptionPane.showMessageDialog(this,
                    "Passport number must follow the pattern 'AB123456' ",
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
            tfPassport.setText("");
            return;
        }
        String continent = (String) comboContinent.getSelectedItem();
        if (continent.equals("Please choose")) {
            JOptionPane.showMessageDialog(this,
                    "Please choose the continent. ",
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        String gender = "";
        if (rbGenderMale.isSelected()) {
            gender = "Male";
        } else if (rbGenderFemale.isSelected()) {
            gender = "Female";
        } else if (rbGenderOther.isSelected()) {
            gender = "N/A - Other";
        }
        HashSet<String> pets = new HashSet<>();
        if (cbPetsFish.isSelected()) {
            pets.add("fish");
        }
        if (cbPetsPig.isSelected()) {
            pets.add("pig");
        }
        if (cbPetsOther.isSelected()) {
            pets.add("other");
        }
        Date date = (Date) ftfBirthdate.getValue();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 || year > 2100) {
            JOptionPane.showMessageDialog(this,
                    "Year should be between 1900 and 2100",
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String entry = name + ";" + passport + ";" + continent + ";" + gender + ";" + pets.toString().replace("[", "").replace("]", "") + ";" + height + ";" + format.format(date);
        lblEntry.setText(entry);
        reset();

    }//GEN-LAST:event_btSubmitActionPerformed

    // to register slider changes
    private void sliderHeightStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderHeightStateChanged
        height = sliderHeight.getValue();
        lblHeightDisplay.setText(height + " cm");
    }//GEN-LAST:event_sliderHeightStateChanged

    // reset all fields
    private void reset() {
        tfName.setText("");
        tfPassport.setText("");
        comboContinent.setSelectedIndex(0);
        btnGroupGender.clearSelection();
        cbPetsFish.setSelected(false);
        cbPetsPig.setSelected(false);
        cbPetsOther.setSelected(false);
        sliderHeight.setValue(170);
        ftfBirthdate.setText("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day07InputValidation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day07InputValidation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day07InputValidation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day07InputValidation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day07InputValidation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btSubmit;
    private javax.swing.ButtonGroup btnGroupGender;
    private javax.swing.JCheckBox cbPetsFish;
    private javax.swing.JCheckBox cbPetsOther;
    private javax.swing.JCheckBox cbPetsPig;
    private javax.swing.JComboBox<String> comboContinent;
    private javax.swing.JFormattedTextField ftfBirthdate;
    private javax.swing.JLabel lblBirthdate;
    private javax.swing.JLabel lblContinent;
    private javax.swing.JLabel lblEntry;
    private javax.swing.JLabel lblGender;
    private javax.swing.JLabel lblHeight;
    private javax.swing.JLabel lblHeightDisplay;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPassport;
    private javax.swing.JLabel lblPets;
    private javax.swing.JRadioButton rbGenderFemale;
    private javax.swing.JRadioButton rbGenderMale;
    private javax.swing.JRadioButton rbGenderOther;
    private javax.swing.JSlider sliderHeight;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfPassport;
    // End of variables declaration//GEN-END:variables
}
