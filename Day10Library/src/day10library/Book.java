/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.util.Comparator;

public class Book {

    int id;
    String isbn;
    String titleAndAuthor;
    byte[] coverImage;

    public Book(int id, String isbn, String titleAndAuthor, byte[] coverImage) {
        this.id = id;
        this.isbn = isbn;
        this.titleAndAuthor = titleAndAuthor;
        this.coverImage = coverImage;
    }

    @Override
    public String toString() {
        return "Book No." + id + ", isbn: " + isbn + ", Title and Author: " + titleAndAuthor;
    }

    public static final Comparator<Book> idComparator = (Book a1, Book a2) -> a1.id - a2.id;

    public static Comparator<Book> isbnComparator = new Comparator<Book>() {
        @Override
        public int compare(Book a1, Book a2) {
            return (int) (a1.isbn.compareTo(a2.isbn));
        }
    };

    public static Comparator<Book> titleComparator = new Comparator<Book>() {
        @Override
        public int compare(Book a1, Book a2) {
            return (int) (a1.titleAndAuthor.compareToIgnoreCase(a2.titleAndAuthor));
        }
    };

}
