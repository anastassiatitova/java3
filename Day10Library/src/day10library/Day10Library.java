/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.List;

import com.opencsv.CSVWriter;
import java.io.Writer;

/**
 *
 * @author garde
 */
public class Day10Library extends javax.swing.JFrame {

    // Variables
    private DefaultListModel<Book> bookListModel = new DefaultListModel<>();
    private Database db;
    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Image files (*.gif, *.png, *.jpg, *.jpeg) ", "gif", "png", "jpg", "jpeg");
    private FileNameExtensionFilter filterCSV = new FileNameExtensionFilter("CSV files ", "csv");
    private BufferedImage currentImage;
    private Book selectedBook;
    private String sortColumn = "id";

    public Day10Library() {
        try {
            initComponents();
//            fileChooserImage.setFileFilter(new FileNameExtensionFilter("Images (*.gif,*.png,*.jpg,*.jpeg)", "gif", "png", "jpg", "jpeg"));
            fileChooserImage.addChoosableFileFilter(filter);
            fileChooserCsv.addChoosableFileFilter(filterCSV);
            db = new Database();
            reloadFromDatabase();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to connect: " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    public void reloadFromDatabase() {
        try {
            ArrayList<Book> bookList = db.getAllBooks(); // get books from database
            switch (sortColumn) {
                case "id":
                    Collections.sort(bookList, Book.idComparator);
                    break;
                case "isbn":
                    Collections.sort(bookList, Book.isbnComparator);
                    break;
                case "title":
                    Collections.sort(bookList, Book.titleComparator);
                    break;
                default:
                    JOptionPane.showMessageDialog(this,
                            "Wrong sort selection",
                            "Sortig error",
                            JOptionPane.ERROR_MESSAGE);
            }
            bookListModel.clear();
            for (Book book : bookList) { // add books to bookListModel to display in JList
                bookListModel.addElement(book);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to connect: " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void clearInputFields() {
        dlgAddEdit_tfIsbn.setText("");
        dlgAddEdit_tfTitle.setText("");
        dlgAddEdit_tfId.setText("-");
        dlgAddEdit_lblImage.setIcon(null);
        currentImage = null;
    }

    // code to resize image - CHECK the ratio
    public static BufferedImage resize(BufferedImage img, int newW) {
        int height = img.getHeight();
        int width = img.getWidth();
        double ratio = (double) height / width;
        int newH = (int) (newW * ratio);
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    // convert image to into byte array to add into database
    byte[] bufferedImageToByteArray(BufferedImage bi) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (!ImageIO.write(bi, "png", baos)) { // ImageIO.write(RenderedImage image, String formatName, File output)
            throw new IOException("Error creating png data");
        }
        byte[] imageBytes = baos.toByteArray();
        return imageBytes;
    }

    // convert byteArray to image for display
    private BufferedImage byteArrayToBufferedImage(byte[] imageData) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        return ImageIO.read(bais);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgAddEdit = new javax.swing.JDialog();
        dlgAddEdit_lblId = new javax.swing.JLabel();
        dlgAddEdit_tfId = new javax.swing.JTextField();
        dlgAddEdit_lblIsbn = new javax.swing.JLabel();
        dlgAddEdit_tfIsbn = new javax.swing.JTextField();
        dlgAddEdit_lblTitle = new javax.swing.JLabel();
        dlgAddEdit_tfTitle = new javax.swing.JTextField();
        dlgAddEdit_lblImage = new javax.swing.JLabel();
        dlgAddEdit_btRemoveImage = new javax.swing.JButton();
        dlgAddEdit_lblDisplayImage = new javax.swing.JLabel();
        dlgAddEdit_btSave = new javax.swing.JButton();
        dlgAddEdit_btCancel = new javax.swing.JButton();
        pMenu = new javax.swing.JPopupMenu();
        pMenu_btEdit = new javax.swing.JMenuItem();
        pMenu_btDelete = new javax.swing.JMenuItem();
        fileChooserImage = new javax.swing.JFileChooser();
        fileChooserCsv = new javax.swing.JFileChooser();
        btGroupSort = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listBooks = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miFile_Export = new javax.swing.JMenuItem();
        miFile_Exit = new javax.swing.JMenuItem();
        miEdit = new javax.swing.JMenu();
        miEdit_AddBook = new javax.swing.JMenuItem();
        miSort = new javax.swing.JMenu();
        miSort_ID = new javax.swing.JRadioButtonMenuItem();
        miSort_ISBN = new javax.swing.JRadioButtonMenuItem();
        miSort_Title = new javax.swing.JRadioButtonMenuItem();

        dlgAddEdit.setResizable(false);

        dlgAddEdit_lblId.setText("ID");

        dlgAddEdit_tfId.setEditable(false);
        dlgAddEdit_tfId.setText("-");

        dlgAddEdit_lblIsbn.setText("ISBN");

        dlgAddEdit_lblTitle.setText("Title and Author");

        dlgAddEdit_lblImage.setText("Cover Image");

        dlgAddEdit_btRemoveImage.setText("Remove Image");
        dlgAddEdit_btRemoveImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btRemoveImageActionPerformed(evt);
            }
        });

        dlgAddEdit_lblDisplayImage.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        dlgAddEdit_lblDisplayImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dlgAddEdit_lblDisplayImageMouseClicked(evt);
            }
        });

        dlgAddEdit_btSave.setText("Save");
        dlgAddEdit_btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btSaveActionPerformed(evt);
            }
        });

        dlgAddEdit_btCancel.setText("Cancel");
        dlgAddEdit_btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgAddEditLayout = new javax.swing.GroupLayout(dlgAddEdit.getContentPane());
        dlgAddEdit.getContentPane().setLayout(dlgAddEditLayout);
        dlgAddEditLayout.setHorizontalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dlgAddEdit_lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_lblIsbn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_lblId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(dlgAddEdit_btSave)))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(dlgAddEdit_tfIsbn, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dlgAddEdit_tfId, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_lblDisplayImage, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_tfTitle))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                        .addComponent(dlgAddEdit_btRemoveImage))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addComponent(dlgAddEdit_btCancel)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        dlgAddEditLayout.setVerticalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_lblId)
                    .addComponent(dlgAddEdit_tfId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dlgAddEdit_lblIsbn)
                    .addComponent(dlgAddEdit_tfIsbn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_lblTitle)
                    .addComponent(dlgAddEdit_tfTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dlgAddEdit_lblImage)
                    .addComponent(dlgAddEdit_lblDisplayImage, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dlgAddEdit_btRemoveImage))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_btSave)
                    .addComponent(dlgAddEdit_btCancel))
                .addGap(23, 23, 23))
        );

        pMenu_btEdit.setText("Edit");
        pMenu_btEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pMenu_btEditActionPerformed(evt);
            }
        });
        pMenu.add(pMenu_btEdit);

        pMenu_btDelete.setText("Delete");
        pMenu_btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pMenu_btDeleteActionPerformed(evt);
            }
        });
        pMenu.add(pMenu_btDelete);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("...");
        getContentPane().add(jLabel1, java.awt.BorderLayout.PAGE_END);

        listBooks.setModel(bookListModel);
        listBooks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listBooksMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                listBooksMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(listBooks);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        miFile.setText("File");

        miFile_Export.setText("Export selected to *.csv");
        miFile_Export.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFile_ExportActionPerformed(evt);
            }
        });
        miFile.add(miFile_Export);

        miFile_Exit.setText("Exit");
        miFile_Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFile_ExitActionPerformed(evt);
            }
        });
        miFile.add(miFile_Exit);

        jMenuBar1.add(miFile);

        miEdit.setText("Edit");

        miEdit_AddBook.setText("Add book");
        miEdit_AddBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miEdit_AddBookActionPerformed(evt);
            }
        });
        miEdit.add(miEdit_AddBook);

        jMenuBar1.add(miEdit);

        miSort.setText("Sort");

        btGroupSort.add(miSort_ID);
        miSort_ID.setSelected(true);
        miSort_ID.setText("by ID");
        miSort_ID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSort_IDActionPerformed(evt);
            }
        });
        miSort.add(miSort_ID);

        btGroupSort.add(miSort_ISBN);
        miSort_ISBN.setText("by ISBN");
        miSort_ISBN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSort_ISBNActionPerformed(evt);
            }
        });
        miSort.add(miSort_ISBN);

        btGroupSort.add(miSort_Title);
        miSort_Title.setText("by Title");
        miSort_Title.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSort_TitleActionPerformed(evt);
            }
        });
        miSort.add(miSort_Title);

        jMenuBar1.add(miSort);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miEdit_AddBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miEdit_AddBookActionPerformed
        // show dialog
        dlgAddEdit.pack();
        dlgAddEdit.setLocationRelativeTo(this);
        dlgAddEdit_btSave.setText("Add book");
        dlgAddEdit.setVisible(true);
        clearInputFields();
        
    }//GEN-LAST:event_miEdit_AddBookActionPerformed

    private void dlgAddEdit_lblDisplayImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dlgAddEdit_lblDisplayImageMouseClicked
        try {
            int returnVal = fileChooserImage.showOpenDialog(this); // display FileChooser ex HeadlessException
            if (returnVal == JFileChooser.APPROVE_OPTION) { // file is selected by user
                File file = fileChooserImage.getSelectedFile();  // get selected file
                currentImage = ImageIO.read(file);  // ex IOException
                currentImage = resize(currentImage, 100);  // resize image
                Icon icon = new ImageIcon(currentImage);
                dlgAddEdit_lblDisplayImage.setIcon(icon);
            }
        } catch (HeadlessException | IOException ex) {
//            lblStatus.setText(ex.getMessage());
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "File error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_dlgAddEdit_lblDisplayImageMouseClicked

    private void dlgAddEdit_btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btSaveActionPerformed
        try {
            ArrayList<String> errorList = new ArrayList<>();  // ArrayList of error messages
            String isbn = dlgAddEdit_tfIsbn.getText();  // to get input from user
            if (!(isbn.matches("^[0-9\\-]{12}[0-9X]{1}$")  || isbn.matches("^[0-9\\-]{9}[0-9X]{1}$"))) {  
                errorList.add("ISBN must be 13 or 10 characters long, require numbers and hyphens only, last character may be X (uppercase)");
            }
            String title = dlgAddEdit_tfTitle.getText();
            if (title.length() < 2 || title.length() > 200) {  // verify the input
                errorList.add("Title and Author are 2-200 characters long.");
            }
            if (!errorList.isEmpty()) {  //display all error messages
                JOptionPane.showMessageDialog(this,
                        String.join("\n", errorList),
                        "Input error(s)",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            // code to insert image
            byte[] imageBytes = null;  // prepare bytes of image for insert
            if (currentImage != null) {
                imageBytes = bufferedImageToByteArray(currentImage); // IOException
            }

            if (dlgAddEdit_btSave.getText().equals("Add book")) {// add new book
                Book book = new Book(0, isbn, title, imageBytes);
                db.addBook(book); // add book to database
            } else if (dlgAddEdit_btSave.getText().equals("Update book record")) { // to update existing 
                int id = Integer.parseInt(dlgAddEdit_tfId.getText());
                Book book = new Book(id, isbn, title, imageBytes);
                db.updateBook(book); // update book in database
            }
            reloadFromDatabase(); // reload the list
            dlgAddEdit.setVisible(false); // set JDialogWindow invisible
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_dlgAddEdit_btSaveActionPerformed

    private void dlgAddEdit_btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btCancelActionPerformed
        dlgAddEdit.setVisible(false);

    }//GEN-LAST:event_dlgAddEdit_btCancelActionPerformed

    private void miSort_IDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSort_IDActionPerformed
        sortColumn = "id";
        reloadFromDatabase();
    }//GEN-LAST:event_miSort_IDActionPerformed

    private void miSort_ISBNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSort_ISBNActionPerformed
        sortColumn = "isbn";
        reloadFromDatabase();
    }//GEN-LAST:event_miSort_ISBNActionPerformed

    private void miSort_TitleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSort_TitleActionPerformed
        sortColumn = "title";
        reloadFromDatabase();
    }//GEN-LAST:event_miSort_TitleActionPerformed

    private void miFile_ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFile_ExitActionPerformed
        dispose();
    }//GEN-LAST:event_miFile_ExitActionPerformed

    private void miFile_ExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFile_ExportActionPerformed
        List<Book> list1 = listBooks.getSelectedValuesList();
        if (list1.size() > 0) {
            try {
                int returnVal = fileChooserCsv.showDialog(this, "Save As");//display FileChooser    ex  HeadlessException
                if (returnVal == JFileChooser.APPROVE_OPTION) { // file is selected by user
                    File file = fileChooserCsv.getSelectedFile(); //get selected file
                    String fileName = file.getCanonicalPath(); // save file name
                    // Important: append file extension if none was given
                    if (!fileName.matches(".*\\.[^.]{1,10}")) {
                        fileName += ".csv";
                        file = new File(fileName);
                    }
                    // perform the export
                    try (Writer writer = new FileWriter(fileName); CSVWriter csvWriter = new CSVWriter(writer)) {
                        String[] headerRecord = {"Id", "ISBN", "Title"};
                        csvWriter.writeNext(headerRecord);
                        for (Book book : list1) { // FIXME: what exception does writeNext throw ??? catch it !
                            csvWriter.writeNext(new String[]{book.id + "", book.isbn, book.titleAndAuthor});
                        }
                    }
                }
            } catch (HeadlessException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Database error",
                        JOptionPane.ERROR_MESSAGE);
            } catch (IOException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Database error",
                        JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(this,
                    "Nothing is selected",
                    "Information message",
                    JOptionPane.PLAIN_MESSAGE);
            return;
        }
        listBooks.clearSelection();

    }//GEN-LAST:event_miFile_ExportActionPerformed

    private void pMenu_btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pMenu_btDeleteActionPerformed
        try {
            int n = JOptionPane.showConfirmDialog(this, "Do you want to delete selected record?\n" + selectedBook.toString(), "Message", JOptionPane.YES_NO_OPTION);
            if (n == JOptionPane.YES_OPTION) {
                db.deleteBook(selectedBook);
                reloadFromDatabase();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_pMenu_btDeleteActionPerformed

    private void pMenu_btEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pMenu_btEditActionPerformed
        try {
            // display Dialog window
            dlgAddEdit.pack();  // set the size of JDialogWindow
            dlgAddEdit.setLocationRelativeTo(this);  // Position
//                dlgAddEdit.setModal(true);
            dlgAddEdit_btSave.setText("Update book record"); // Change text of Save button
            dlgAddEdit.setVisible(true);  // make JDialogWindow visible
            clearInputFields(); // clear input fields

            //display selected book
            dlgAddEdit_tfId.setText(selectedBook.id + "");
            dlgAddEdit_tfIsbn.setText(selectedBook.isbn);
            dlgAddEdit_tfTitle.setText(selectedBook.titleAndAuthor);
            byte[] image = db.getImageByID(selectedBook.id);
            // to get Icon from Byte[]
            if (image != null) {
                currentImage = byteArrayToBufferedImage(image); // to convert byte[] to image
                currentImage = resize(currentImage, 150);  // resize image
                Icon icon = new ImageIcon(currentImage);
                dlgAddEdit_lblImage.setIcon(icon);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_pMenu_btEditActionPerformed

    private void dlgAddEdit_btRemoveImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btRemoveImageActionPerformed
        dlgAddEdit_lblDisplayImage.setIcon(null);
    }//GEN-LAST:event_dlgAddEdit_btRemoveImageActionPerformed

    private void listBooksMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listBooksMouseReleased
        if (evt.getClickCount() == 2) {
            selectedBook = listBooks.getSelectedValue();
            listBooks.clearSelection();
            try {
                // display Dialog window
                dlgAddEdit.pack();  // set the size of JDialogWindow
                dlgAddEdit.setLocationRelativeTo(this);  // Position
//                dlgAddEdit.setModal(true);
                dlgAddEdit_btSave.setText("Update book record"); // Change text of Save button
                dlgAddEdit.setVisible(true);  // make JDialogWindow visible
                clearInputFields(); // clear input fields

                //display selected book
                dlgAddEdit_tfId.setText(selectedBook.id + "");
                dlgAddEdit_tfIsbn.setText(selectedBook.isbn);
                dlgAddEdit_tfTitle.setText(selectedBook.titleAndAuthor);
                byte[] image = db.getImageByID(selectedBook.id);
                // to get Icon from Byte[]
                if (image != null) {
                    currentImage = byteArrayToBufferedImage(image); // to convert byte[] to image
                    currentImage = resize(currentImage, 150);  // resize image
                    Icon icon = new ImageIcon(currentImage);
                    dlgAddEdit_lblDisplayImage.setIcon(icon);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Database error",
                        JOptionPane.ERROR_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Database error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_listBooksMouseReleased

    private void listBooksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listBooksMouseClicked
        if (SwingUtilities.isRightMouseButton(evt)) {
            int row = listBooks.locationToIndex(evt.getPoint());
            listBooks.setSelectedIndex(row);
            selectedBook = listBooks.getSelectedValue();
            pMenu.show(this, evt.getX(), evt.getY());
            listBooks.clearSelection();
        }      
 
    }//GEN-LAST:event_listBooksMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day10Library().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btGroupSort;
    private javax.swing.JDialog dlgAddEdit;
    private javax.swing.JButton dlgAddEdit_btCancel;
    private javax.swing.JButton dlgAddEdit_btRemoveImage;
    private javax.swing.JButton dlgAddEdit_btSave;
    private javax.swing.JLabel dlgAddEdit_lblDisplayImage;
    private javax.swing.JLabel dlgAddEdit_lblId;
    private javax.swing.JLabel dlgAddEdit_lblImage;
    private javax.swing.JLabel dlgAddEdit_lblIsbn;
    private javax.swing.JLabel dlgAddEdit_lblTitle;
    private javax.swing.JTextField dlgAddEdit_tfId;
    private javax.swing.JTextField dlgAddEdit_tfIsbn;
    private javax.swing.JTextField dlgAddEdit_tfTitle;
    private javax.swing.JFileChooser fileChooserCsv;
    private javax.swing.JFileChooser fileChooserImage;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<Book> listBooks;
    private javax.swing.JMenu miEdit;
    private javax.swing.JMenuItem miEdit_AddBook;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miFile_Exit;
    private javax.swing.JMenuItem miFile_Export;
    private javax.swing.JMenu miSort;
    private javax.swing.JRadioButtonMenuItem miSort_ID;
    private javax.swing.JRadioButtonMenuItem miSort_ISBN;
    private javax.swing.JRadioButtonMenuItem miSort_Title;
    private javax.swing.JPopupMenu pMenu;
    private javax.swing.JMenuItem pMenu_btDelete;
    private javax.swing.JMenuItem pMenu_btEdit;
    // End of variables declaration//GEN-END:variables
}
