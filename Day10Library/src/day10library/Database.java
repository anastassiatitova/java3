/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author garde
 */
public class Database {

    ArrayList<Book> bookList = new ArrayList<>();
    private static String dbUrl = "jdbc:mysql://localhost:3306/day10library";
    private static String username = "root";
    private static String password = "Sdf93jka63>";

    private Connection conn = null; // make connection private

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbUrl, username, password);
    }

    public ArrayList<Book> getAllBooks() throws SQLException {
        ArrayList<Book> list = new ArrayList<>();
        String sql = "SELECT id, isbn, titleAndAuthor  FROM book";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a godd practice to use try-with-resources for ResultSet so it is frred up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String isbn = result.getString("isbn");
                String titleAndAuthor = result.getString("titleAndAuthor");
                // we do not fetch image here on purpose
                list.add(new Book(id, isbn, titleAndAuthor, null));
            }
        }
        return list;
    }

    // fetch one record including the blob
    public Book getBookById(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM book WHERE id=?");
        statement.setInt(1, id);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                String isbn = result.getString("isbn");
                String titleAndAuthor = result.getString("titleAndAuthor");
                byte[] coverImage = result.getBytes("coverImage");
                return new Book(id, isbn, titleAndAuthor, coverImage);
            } else {
                throw new SQLException("Record not found");
            }
        }
    }

    public void addBook(Book book) throws SQLException {
        String sql = "INSERT INTO book VALUES (NULL,?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, book.isbn);
        statement.setString(2, book.titleAndAuthor);
        statement.setBytes(3, book.coverImage);
        statement.executeUpdate();
        System.out.println("Record inserted");
    }

    public byte[] getImageByID(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT coverImage FROM book  WHERE id=?");
        statement.setInt(1, id);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                return result.getBytes("coverImage");
            } else {
                throw new SQLException("Record not found");
            }
        }
    }

    public void updateBook(Book book) throws SQLException {
        String sql = "UPDATE book SET isbn=?, titleAndAuthor=?, coverImage=? WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, book.isbn);
        statement.setString(2, book.titleAndAuthor);
        statement.setBytes(3, book.coverImage);
        statement.setInt(4, book.id);
        statement.executeUpdate();
        System.out.println("Record updated");
    }

    public void deleteBook(Book book) throws SQLException {
        String sql = "DELETE FROM book WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, book.id);
        statement.executeUpdate();
        System.out.println("Record deleted id =" + book.id);
    }
}
