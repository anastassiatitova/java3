/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09tododb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class Todo {
    
    private int id;
    private String task;
    private int difficulty;
    private Date dueDate;
    private Status status;
    
    enum Status {
        PENDING, DONE, DELEGATED
    };
    
    public Todo(int id, String task, int difficulty, Date dueDate, Status status) throws DataInvalidException {
        this.id = id;
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate);
        setStatus(status);
    }
    
    public int getId() {
        return id;
    }
    
    public String getTask() {
        return task;
    }
    
    public void setTask(String task) throws DataInvalidException {
        if (!task.matches("^[a-zA-Z0-9\\s\\-*?%#@()\\.\\,\\/\\\\]{1,100}$")) {
            throw new DataInvalidException("Task can have 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\\ only.");
        }
        this.task = task;
    }
    
    public int getDifficulty() {
        return difficulty;
    }
    
    public void setDifficulty(int difficulty) throws DataInvalidException {
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        if (!set.contains(difficulty)) {
            throw new DataInvalidException("Difficulty is between 1 and 5.");
        }
        this.difficulty = difficulty;
    }
    
    public Date getDueDate() {
        return dueDate;
    }
    
    public void setDueDate(Date dueDate) throws DataInvalidException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dueDate);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 || year > 2100) {
            throw new DataInvalidException("Year should be between 1900 and 2100.");
        }
        this.dueDate = dueDate;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return "Todo No " + id + ": " + task + ", difficulty: " + difficulty + ", dueDate: " + formatter.format(dueDate) + ", status: " + status;
    }
    
}
