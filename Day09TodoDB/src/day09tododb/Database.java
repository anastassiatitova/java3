/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09tododb;



import day09tododb.Todo.Status;
import java.beans.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import jdk.jshell.Snippet;


public class Database {

    private static String dbUrl = "jdbc:mysql://localhost:3306/day09todos";
    private static String username = "root";
    private static String password = "Sdf93jka63>";

    private Connection conn = null; // make connection private

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbUrl, username, password);
    }

    public ArrayList<Todo> getAllTodos() throws SQLException, DataInvalidException, IllegalArgumentException {
        ArrayList<Todo> list = new ArrayList<>();
        String sql = "SELECT * FROM todo";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a godd practice to use try-with-resources for ResultSet so it is frred up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String task = result.getString("task");
                int difficulty = result.getInt("difficulty");
                Date dueDate = result.getDate("dueDate");
                String str = result.getString("status");
                Status st = Status.valueOf(str); // ex IllegalArgumentException
                list.add(new Todo(id, task, difficulty, dueDate, st));
            }
        }
        return list;
    }

    public void addTodo(Todo todo) throws SQLException, DataInvalidException {
        String sql = "INSERT INTO todo VALUES (NULL, ?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS); // TO CHECK
        statement.setString(1, todo.getTask());
        statement.setInt(2, todo.getDifficulty());
        java.sql.Date sqlDate = new java.sql.Date(todo.getDueDate().getTime());
        statement.setDate(3, sqlDate);
        statement.setString(4, todo.getStatus().toString());
        statement.executeUpdate();
        
        ResultSet rs = statement.getGeneratedKeys();
        System.out.println("Record inserted");
    }

    public void updateTodo(Todo todo) throws SQLException, DataInvalidException {
        String sql = "UPDATE todo SET task=?, difficulty=?, dueDate=?, status=? WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, todo.getTask());
        statement.setInt(2, todo.getDifficulty());
        java.sql.Date sqlDate = new java.sql.Date(todo.getDueDate().getTime());
        statement.setDate(3, sqlDate);
        statement.setString(4, todo.getStatus().toString());
        statement.setInt(5, todo.getId());
        statement.executeUpdate();
        System.out.println("Record updated: " + todo.getId());
    }

    public void deleteTodo(Todo todo) throws SQLException {
        String sql = "DELETE FROM todo WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, todo.getId());
        statement.executeUpdate();
        System.out.println("Record deleted id =" + todo.getId());
    }
}
