package day02todos;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.*;
import java.time.*;

public class Todo {

    private String task;
    private LocalDate dueDate;
    private int hoursOfWork;
    private TaskStatus status;
    static int count;
        
    enum TaskStatus {
        pending,
        done
    };

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Todo.count = count;
    }

    
    // constructor - every parameter is entered seperatly
    public Todo(String task, LocalDate dueDate, int hoursOfWork) {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
        setStatus(status.pending);
        count++;
    }

    // constructor - all paramters are entered as one string
    public Todo(String dataLine) throws DateTimeException, NumberFormatException, IllegalArgumentException, NullPointerException, ArrayIndexOutOfBoundsException {
        String[] str = dataLine.split(";", 4);  //split data string  - PatternSyntaxException
        dueDate = LocalDate.parse(str[1], DateTimeFormatter.ofPattern("yyyy-MM-dd")); // convert string to LocalDate - DateTimeException
        int hours = Integer.parseInt(str[2]); // convert string to int - NumberFormatException
        status = TaskStatus.valueOf(str[3]); // convert string to enum -  IllegalArgumentException, NullPointerException
        setTask(str[0]); // ex IllegalArgumentException
        setDueDate(dueDate);
        setHoursOfWork(hours);
        setStatus(status);
        count++;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) throws PatternSyntaxException {
        // to create pattern of characters to compare with task - PatternSyntaxException
        Pattern pattern = Pattern.compile("[^;|`]*");
        Matcher matcher = pattern.matcher(task);
        
        //check for task constraints
        if (task.length() < 2 || task.length() > 50 || !matcher.matches()) {
//        if (!task.matches("[^;|`]*{2, 50}")) {
            throw new IllegalArgumentException("Task should be between 2 and 50 characters long and does not include [ ; | `]");
        }
        this.task = task;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        // check dueDate constraints 
        if (dueDate.getYear() < 1900 || dueDate.getYear() > 2100) {
            throw new IllegalArgumentException("DueDate should be between 1900 and 2100");
        }
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) {
        // check hoursOfWork constraints
        if (hoursOfWork < 0) {
            throw new IllegalArgumentException("Hours of work should be more or equal 0.");
        }
        this.hoursOfWork = hoursOfWork;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return task + ", " + dueDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")) + ", will take " + hoursOfWork + " hour(s) of work, " + status;
    }

    public String toDataString() {
        return task + ";" + dueDate.toString() + ";" + hoursOfWork + ";" + status;
    }

}
