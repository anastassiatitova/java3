package day02todos;

import day02todos.Todo.TaskStatus;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.regex.PatternSyntaxException;

public class Day02Todos {

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static final String FILE_NAME = "todos.txt";
    static final int DEFAULT_CHOICE = 10;

    public static int displayMenu() {
        System.out.println("");
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add a todo\n"
                + "2. List all todos (numbered)\n"
                + "3. Delete a todo\n"
                + "4. Modify a todo\n"
                + "0. Exit\n"
                + "Your choice is:");
        int choice = inputInt(); // user input int 
        input.nextLine();
        return choice; // return user's choice
    }

    public static void addTodo() {
        try {
            // data input from a user
            System.out.print("Enter task description: ");
            String task = input.nextLine();
            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String date = input.nextLine();
            System.out.print("Enter hours of work (integer): ");
            int hours = inputInt(); // user input - int - InputMismatchException
            input.nextLine();

            LocalDate dueDate = stringToLocalDate(date); // convert string to LocalDate
            Todo td = new Todo(task, dueDate, hours); // create new Todo - IllegalArgumentException
            todoList.add(td); // add new Todo to the list

            //print created new Todo
            System.out.println("You've created the following todo: ");
            System.out.println(td.toString());
        } catch (IllegalArgumentException ex) {
            System.out.println("Error: wrong input. " + ex.getMessage());
        }
    }

    public static LocalDate stringToLocalDate(String str) {
        LocalDate dueDate = LocalDate.of(2900, 01, 01); // set default date in case of the exception
        try {
            if (str.contains("/")) { // verify if string contains '/'
                dueDate = LocalDate.parse(str, DateTimeFormatter.ofPattern("yyyy/MM/dd")); // convert string to LocalDate - DateTimeException
                return dueDate; // return user's date
            } else {
                System.out.println("Error: the date should be YYYY/MM/DD. "); // error message if string does not have '/'
            }
        } catch (DateTimeException ex) {
            System.out.println("Error: the date should be YYYY/MM/DD. " + ex.getMessage());
        }
        return dueDate; // return default date in case of exception
    }

    public static void listTodos() {
        System.out.println("Listing all todos.");
        if (todoList.size() > 0) { // check if Todo list is empty
            for (int i = 0; i < todoList.size(); i++) {
                System.out.println("#" + (i + 1) + ": " + todoList.get(i).toString());
            }
        } else { // message if todo list is empty
            System.out.println("There are no todos.");
        }
    }

    public static void deleteTodo() {
        try {
            System.out.print("Deleting a todo. Which todo # would you like to delete? ");
            int num = inputInt(); // user input - int - InputMismatchException
            input.nextLine();

            todoList.remove(num - 1); // remove selected todo - IndexOutOfBoundsException
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Error: Todo does not exist. " + ex.getMessage());
        }
    }

    public static void modifyTodo() {
        try {
            // ask user for todo number
            System.out.print("Modifying a todo. Which todo # would you like to modify?");
            int num = inputInt();
            input.nextLine();
            // new info from user
            System.out.println("Modifying todo #" + num + ":" + todoList.get(num - 1).toString());
            System.out.print("Enter new task description: ");
            String task = input.nextLine();
            System.out.print("Enter new due Date (yyyy/mm/dd): ");
            String date = input.nextLine();
            System.out.print("Enter new hours of work (integer): ");
            int hours = inputInt(); // user input - int - InputMismatchException
            input.nextLine();
            System.out.print("Enter if task is 'Done' or 'Pending': ");
            String status = input.nextLine();
            status.toLowerCase(); // to remove capital letters from string

            LocalDate dueDate = stringToLocalDate(date); // convert string to LocalDate

            // modify existing todo entry
            if (!task.isBlank()) { // check if task is empty string
                todoList.get(num - 1).setTask(task);
            }
            todoList.get(num - 1).setDueDate(dueDate);
            todoList.get(num - 1).setHoursOfWork(hours);
            todoList.get(num - 1).setStatus(TaskStatus.valueOf(status)); // convert string to enum - IllegalArgumentException, NullPointerException

            System.out.println("You've modified todo #1 as follows:");
            System.out.println(todoList.get(num - 1).toString());
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Error: Todo does not exist. " + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            System.out.println("Error: wrong input. " + ex.getMessage());
        } catch (NullPointerException ex) {
            System.out.println("Error: wrong staus. " + ex.getMessage());
        }
    }

    static void loadDataFromFile() {
        try (Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                Todo td = new Todo(fileInput.nextLine());
                todoList.add(td);
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());

        } catch (ArrayIndexOutOfBoundsException | DateTimeException | IllegalArgumentException | NullPointerException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    static void saveDataToFile() {
        try (PrintWriter output = new PrintWriter(new File(FILE_NAME))) {
            for (Todo td : todoList) {
                output.println(td.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    static int inputInt() {
        while (true) {
            try {
                int num = input.nextInt();
                return num;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid integer, please try again: ");
                input.next(); // consume invalid input
            }
        }
    }

    public static void main(String[] args) {
        loadDataFromFile(); // read froom the file
        boolean check = true;
        while (check) {
            int choice = displayMenu();  // display menu
            switch (choice) {
                case 0: { // save data and exit
                    saveDataToFile();
                    System.out.println("Exiting. Good bye!");
                    check = false;
                    break;
                }
                case 1: // add a new todo 
                    addTodo();
                    break;
                case 2: // display all todos
                    listTodos();
                    break;
                case 3: // delete a todo
                    deleteTodo();
                    break;
                case 4: // modify a todo
                    modifyTodo();
                    break;
                default: // invalid user's choice
                    System.out.println("This is not a valid Menu Option! Please Select Another");
            }
        }

    }

}
