/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02todos;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author garde
 */
public class TodoTest {
    
//    public TodoTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }

    /**
     * Test of getTask method, of class Todo.
     */
    @Test
    public void testGetTask() {
        System.out.println("getTask");
        Todo instance = null;
        String expResult = "";
        String result = instance.getTask();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTask method, of class Todo.
     */
    @Test
    public void testSetTask() {
        System.out.println("setTask");
        String task = "";
        Todo instance = null;
        instance.setTask(task);
        fail("The test case is a prototype.");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentException() {
        System.out.println("Todo.setDueDate(invalid) ");
        LocalDate date = LocalDate.of(2200, 02, 27);
        Todo instance = new Todo("Test", date, 3);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDueDateManyIllegalArgumentException() {
        LocalDate date1;
        String [] datesValid = { "1900-01-01", "2001-05-11", "2100-12-31"};
        String [] datesInvalid = { "1800-01-01", "2200-05-11", "2000-12-31"};
        System.out.println("Todo.setDueDate(invalid) ");
        for (int i = 0; i < datesInvalid.length; i++) {
             String string = datesInvalid[i];
            date1 = LocalDate.parse(string, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
             Todo instance = new Todo("Test", date1, 3);
             fail("The test case failed.");
        }
     
    }
    
    /**
     * Test of getDueDate method, of class Todo.
     */
    @Test
    public void testGetDueDate() {
        System.out.println("getDueDate");
        Todo instance = null;
        LocalDate expResult = null;
        LocalDate result = instance.getDueDate();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDueDate method, of class Todo.
     */
    @Test
    public void testSetDueDate() {
        System.out.println("setDueDate");
        LocalDate dueDate = null;
        Todo instance = null;
        instance.setDueDate(dueDate);
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHoursOfWork method, of class Todo.
     */
    @Test
    public void testGetHoursOfWork() {
        System.out.println("getHoursOfWork");
        Todo instance = null;
        int expResult = 0;
        int result = instance.getHoursOfWork();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of setHoursOfWork method, of class Todo.
     */
    @Test
    public void testSetHoursOfWork() {
        System.out.println("setHoursOfWork");
        int hoursOfWork = 0;
        Todo instance = null;
        instance.setHoursOfWork(hoursOfWork);
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStatus method, of class Todo.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        Todo instance = null;
        Todo.TaskStatus expResult = null;
        Todo.TaskStatus result = instance.getStatus();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStatus method, of class Todo.
     */
    @Test
    public void testSetStatus() {
        System.out.println("setStatus");
        Todo.TaskStatus status = null;
        Todo instance = null;
        instance.setStatus(status);
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Todo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Todo instance = null;
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of toDataString method, of class Todo.
     */
    @Test
    public void testToDataString()throws ParseException, IllegalArgumentException {
        System.out.println("toDataString");
        Todo instance = new Todo("Run this test", LocalDate.now(),3);
        String expResult = "Run this test;2021-03-02;3;pending";
        String result = instance.toDataString();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDataLineConstructor () {
        System.out.println("Todo(String dataLine) ");
        Todo instance = new Todo("Run this test;2021-03-02;3;pending");
        LocalDate date = LocalDate.now();
        assertEquals(date, instance.getDueDate());
        assertEquals(3, instance.getHoursOfWork());
        assertEquals(Todo.TaskStatus.pending, instance.getStatus());
    }
}
