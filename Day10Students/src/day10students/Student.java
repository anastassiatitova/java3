/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10students;

import java.util.Comparator;

public class Student {

    int id;
    String name;
    byte[] image; // other possible types ByteArray, Blob, a data stream

    public Student(int id, String name, byte[] image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Student: " + "id: " + id + ", name: " + name;
    }

    public static Comparator<Student> nameComparator = new Comparator<Student>() {
        @Override
        public int compare(Student a1, Student a2) {
            return (int) (a1.name.compareTo(a2.name));
        }
    };

    public static final Comparator<Student> idComparator = (Student a1, Student a2) -> a1.id - a2.id;

/*    public static Comparator<Student> idComparator = new Comparator<Student>() {
        @Override
        public int compare(Student a1, Student a2) {
//            return Integer.compare(a1.id, a2.id);
            return a1.id - a2.id;
        }
    }; */

}
