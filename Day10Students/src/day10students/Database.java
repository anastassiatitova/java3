/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10students;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author garde
 */
public class Database {

    ArrayList<Student> studentList = new ArrayList<>();
    private static String dbUrl = "jdbc:mysql://localhost:3306/day10students";
    private static String username = "root";
    private static String password = "Sdf93jka63>";

    private Connection conn = null; // make connection private

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbUrl, username, password);
    }

    public ArrayList<Student> getAllStudents() throws SQLException {
        ArrayList<Student> list = new ArrayList<>();
        String sql = "SELECT id, name FROM students";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a godd practice to use try-with-resources for ResultSet so it is frred up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String name = result.getString("name");
                // we do not fetch image here on purpose
                list.add(new Student(id, name, null));
            }
        }
        return list;
    }

    // fetch one record including the blob
    public Student getStudentById(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM students WHERE id=?");
        statement.setInt(1, id);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                String name = result.getString("name");
                byte[] image = result.getBytes("image");
                return new Student(id, name, image);
            } else {
                throw new SQLException("Resord not found");
            }
        }
    }

    public void addStudent(Student student) throws SQLException {
        String sql = "INSERT INTO students VALUES (NULL,?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, student.name);
        statement.setBytes(2, student.image);
        statement.executeUpdate();
        System.out.println("Record inserted");
    }

    public byte[] getImageByID(Student student) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT image FROM students WHERE id=?");
        statement.setInt(1, student.id);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                return result.getBytes("image");
            } else {
                throw new SQLException("Resord not found");
            }
        }
    }

    public void updateStudent(Student student) throws SQLException {
        String sql = "UPDATE students SET name=?, image=? WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, student.name);
        statement.setBytes(2, student.image);
        statement.setInt(3, student.id);
        statement.executeUpdate();
        System.out.println("Record updated");
    }

    public void deleteStudent(Student student) throws SQLException {
        String sql = "DELETE FROM students WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, student.id);
        statement.executeUpdate();
        System.out.println("Record deleted id =" + student.id);
    }
}
