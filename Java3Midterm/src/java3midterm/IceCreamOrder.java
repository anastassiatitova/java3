/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java3midterm;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;

public class IceCreamOrder {

    private String customerName;
    private LocalDateTime deliveryDate;  // Teacher agreed to use LOCALDATETIME
    private ArrayList<Flavour> flavlist = new ArrayList<>();

    public enum Flavour {
        VANILLA, CHOCOLATE, STRAWBERRY, ROCKYROAD
    };

    public IceCreamOrder(String customerName, LocalDateTime deliveryDate, ArrayList<Flavour> flavList) throws DataInvalidException {
        setCustomerName(customerName);
        setDeliveryDate(deliveryDate);
        setFlavlist(flavList);
    }

    public IceCreamOrder(String dataLine) throws DataInvalidException {
        try {
            // parse string read from file
            String[] str = dataLine.split(";"); // PatternSyntaxException (sub of IllegalArgumentException)
            // conver string to date
            LocalDateTime date = LocalDateTime.parse(str[1]); // ex DateTimeException 
            // convert string to ArrayList of enums
            String[] flavours = str[2].split(",");
            for (String s : flavours) {
                s = s.trim().toUpperCase();
                Flavour r = Flavour.valueOf(s);
                flavlist.add(r);  // ex IllegalArgumentException, NullPointerException
            }
            setCustomerName(str[0]);
            setDeliveryDate(date);
            setFlavlist(flavlist);
        } catch (IllegalArgumentException | NullPointerException | DateTimeException ex) {
            throw new DataInvalidException(ex.getMessage());
        }
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) throws DataInvalidException {
        if (!customerName.matches("^[a-zA-Z0-9\\s\\-\\,\\.\\(\\)\\'\\\"]*$")) {
            throw new DataInvalidException("In the name only allowed: letters, numbers,empty spaces, dashes, commas, dots, parentheses, single and double quotes.");
        }
        this.customerName = customerName;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) throws DataInvalidException {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime plusHundred = today.plusDays(100);
        if (deliveryDate.isBefore(today) || deliveryDate.isAfter(plusHundred)) {
            throw new DataInvalidException("Appointment can only be made for tomorrow or a later date and minutes can be only: 00, 20, 40.");

        }
        this.deliveryDate = deliveryDate;
    }

    public ArrayList<Flavour> getFlavlist() {
        return flavlist;
    }

    public void setFlavlist(ArrayList<Flavour> flavlist) throws DataInvalidException {
        if (flavlist.size() == 0) {
            throw new DataInvalidException("Flavour list can not be empty.");
        }
        this.flavlist = flavlist;
    }

    @Override
    public String toString() {
        return "For " + customerName + ", on " + deliveryDate.toLocalDate().toString() + " at " + deliveryDate.toLocalTime().toString() + ", falvours: " + flavlist.toString().replace("[", "").replace("]", "");
    }

    public String toDataString() {
        return customerName + ";" + deliveryDate + ";" + flavlist.toString().replace("[", "").replace("]", "");
    }

    public static Comparator<IceCreamOrder> nameComparator = new Comparator<IceCreamOrder>() {
        @Override
        public int compare(IceCreamOrder a1, IceCreamOrder a2) {
            return (int) (a1.getCustomerName().compareTo(a2.getCustomerName()));
        }
    };

    public static Comparator<IceCreamOrder> dateComparator = new Comparator<IceCreamOrder>() {
        @Override
        public int compare(IceCreamOrder a1, IceCreamOrder a2) {
            return (int) (a1.getDeliveryDate().compareTo(a2.getDeliveryDate()));
        }
    };
}
