/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java3midterm;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;
import java3midterm.IceCreamOrder.Flavour;

public class Application {

    static ArrayList<IceCreamOrder> ordersList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static final String FILE_NAME = "orders.txt";

    public static void readFromFile() {
        try (Scanner inputFile = new Scanner(new File(FILE_NAME))) { // ex IOException
            while (inputFile.hasNextLine()) {
                IceCreamOrder app = new IceCreamOrder(inputFile.nextLine()); // ex DataInvalidException
                ordersList.add(app);
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        } catch (DataInvalidException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public static int saveDataToFile() {
        int count = 0;
        try (PrintWriter output = new PrintWriter(new File(FILE_NAME))) { // ex IOException
            for (IceCreamOrder td : ordersList) {
                output.println(td.toDataString());
                count++;
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return count;
    }

    public static int menu() {
        System.out.println("");
        System.out.print("Available options\n"
                + "1. Add an order\n"
                + "2. List orders by customer name\n"
                + "3. List orders by delivery date\n"
                + "0. Exit\n"
                + "Please enter your choice:");
        int choice = inputInt(); // user input int 
        return choice; // return user's choice
    }

    public static void addOrder() {
        try {
            System.out.println("Adding an order.");
            System.out.print("Please enter customer's name: ");
            String customerName = input.nextLine();
            System.out.print("Enter the delivery date (yyyy-mm-dd):");
            LocalDate date = inputDate();
            System.out.print("Enter delivery time (hh:mm): ");
            LocalTime time = inputTime();
            System.out.print("Available flavours:");
            for (Flavour f : Flavour.values()) {
                System.out.print(f + " ");
            }
            System.out.println("");
            boolean check = true;
            ArrayList<Flavour> flavlist = new ArrayList<>();
            while (check) {

                System.out.print("Enter which flavour to add, empty to finish: ");
                String str = input.nextLine();
                if (!str.isBlank()) {
                    str = str.toUpperCase().trim();
                    try {
                        Flavour f = Flavour.valueOf(str); //ex IllegalArgumentException, NullPointerException
                        flavlist.add(f);
                    } catch (IllegalArgumentException | NullPointerException ex) {
                        System.out.println("No such flavour. Try again. ");
                    }

                } else {
                    check = false;

                }
            }
            LocalDateTime deliveryDate = LocalDateTime.of(date, time);
            IceCreamOrder order = new IceCreamOrder(customerName, deliveryDate, flavlist); // ex DataInvalidException
            ordersList.add(order);
            System.out.println("Order accepted:");
            System.out.println(order.toString());

        } catch (NullPointerException | DataInvalidException ex) {
            System.out.println(ex.getMessage());
        }
    }

    static int inputInt() {
        while (true) {
            try {
                int num = input.nextInt();
                input.nextLine();
                return num;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid integer, please try again: ");
                input.next(); // consume invalid input
            }
        }
    }

    static LocalDate inputDate() {
        
         while (true) {
            try {
                String str = input.nextLine();
                LocalDate date = LocalDate.parse(str); // ex IllegalArgumentException, DateTimeParseException
                return date;
            } catch (IllegalArgumentException | DateTimeParseException ex) {
                System.out.print("Invalid date, please try again: ");
            }
        }
    }

    static LocalTime inputTime() {
        while (true) {
            try {
                String str = input.nextLine();
                LocalTime time = LocalTime.parse(str);// ex IllegalArgumentException,DateTimeParseException
                return time;
            } catch (IllegalArgumentException | DateTimeParseException ex) {
                System.out.print("Invalid time, please try again: ");
            }
        }
    }

    public static void sortByName() {
        Collections.sort(ordersList, IceCreamOrder.nameComparator);
        for (IceCreamOrder a : ordersList) {
            System.out.println(a.toString());
        }
    }

    public static void sortByDate() {
        Collections.sort(ordersList, IceCreamOrder.dateComparator);
        for (IceCreamOrder a : ordersList) {
            System.out.println(a.toString());
        }
    }

    public static void main(String[] args) {
        readFromFile();

        for (IceCreamOrder a : ordersList) { // to check the file 
            System.out.println(a.toString());
        }

        while (true) {
            int choice = menu();
            switch (choice) {
                case 0: { // save data and exit
                    int count = saveDataToFile();
                    System.out.printf("Data saved, total of %d orders. Exiting", count);
                    return;
                }
                case 1: // make a new order
                    addOrder();
                    break;
                case 2:  // sort all by customer name
                    sortByName();
                    break;
                case 3: // sort all by date
                    sortByDate();
                    break;
                default: // invalid user's choice
                    System.out.println("This is not a valid Menu Option! Please Select Another");
            }
        }
    }

}
