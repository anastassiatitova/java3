/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java3midterm;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author garde
 */
public class IceCreamOrderTest {

//    public IceCreamOrderTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
    @Test(expected = DataInvalidException.class)
    public void testDataInvalidExceptionSetCustomerName() throws DataInvalidException {
        System.out.println("getCustomerName");
        LocalDateTime deliveryDate = LocalDateTime.of(2021, 03, 05, 10, 15);
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        flavlist.add(IceCreamOrder.Flavour.VANILLA);
        IceCreamOrder instance = new IceCreamOrder("John Smith", deliveryDate, flavlist);
        String customerName = "John@ong";
        String customerName1 = "John<>Wong";
        instance.setCustomerName(customerName);
        instance.setCustomerName(customerName1);
    }

    @Test
    public void testSetCustomerName() throws DataInvalidException {
        System.out.println("setCustomerName");
        LocalDateTime deliveryDate = LocalDateTime.of(2021, 03, 05, 10, 15);
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        flavlist.add(IceCreamOrder.Flavour.VANILLA);
        IceCreamOrder instance = new IceCreamOrder("John Smith", deliveryDate, flavlist);
        String customerName = "John Wong";
        String customerName1 = "John-Wong";
        instance.setCustomerName(customerName);
        instance.setCustomerName(customerName1);

    }

    @Test
    public void testSetDeliveryDate() throws Exception {
        System.out.println("setDeliveryDate");
        LocalDateTime dt = LocalDateTime.now();
        LocalDateTime deliveryDate = LocalDateTime.now().plusMinutes(10);
        LocalDateTime deliveryDate1 = LocalDateTime.now().plusDays(99);
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        flavlist.add(IceCreamOrder.Flavour.VANILLA);
        IceCreamOrder instance = new IceCreamOrder("John Smith", dt, flavlist);
        instance.setDeliveryDate(deliveryDate);
        instance.setDeliveryDate(deliveryDate1);

    }

    @Test(expected = DataInvalidException.class)
    public void testDataInvalidExceptionSetDeliveryDate() throws DataInvalidException {
        System.out.println("getCustomerName");
        LocalDateTime dt = LocalDateTime.now();
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        flavlist.add(IceCreamOrder.Flavour.VANILLA);
        IceCreamOrder instance = new IceCreamOrder("John Smith", dt, flavlist);
        LocalDateTime deliveryDate = LocalDateTime.now().minusMinutes(5);
        LocalDateTime deliveryDate1 = LocalDateTime.now().plusDays(101);
        instance.setDeliveryDate(deliveryDate);
        instance.setDeliveryDate(deliveryDate1);
    }

    @Test
    public void testToString() throws DataInvalidException {
        System.out.println("toString");
        LocalDateTime dt = LocalDateTime.of(2021, 03, 05, 10, 05);
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        flavlist.add(IceCreamOrder.Flavour.VANILLA);
        IceCreamOrder instance = new IceCreamOrder("John Smith", dt, flavlist);
        String expResult = "For John Smith, on 2021-03-05 at 10:05, falvours: VANILLA";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    @Test
    public void testToDataString() throws DataInvalidException {
        System.out.println("toDataString");
        LocalDateTime dt = LocalDateTime.of(2021, 03, 05, 10, 05);
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        flavlist.add(IceCreamOrder.Flavour.VANILLA);
        IceCreamOrder instance = new IceCreamOrder("John Smith", dt, flavlist);
        String expResult = "John Smith;2021-03-05T10:05;VANILLA";
        String result = instance.toDataString();
        assertEquals(expResult, result);
    }

    @Test
    public void testIceCreamOrder() throws DataInvalidException {

       String dataLine = "Mike Br;2021-03-05T10:05;VANILLA";
        String[] str = dataLine.split(";"); 
       
        LocalDateTime date1 = LocalDateTime.parse(str[1]); // ex DateTimeException 
        // convert string to ArrayList of enums
        String[] flavours = str[2].split(",");
        ArrayList<IceCreamOrder.Flavour> flavlist = new ArrayList<>();
        for (String s : flavours) {
            s = s.trim().toUpperCase();
            IceCreamOrder.Flavour r = IceCreamOrder.Flavour.valueOf(s);
            flavlist.add(r);  // ex IllegalArgumentException, NullPointerException
        }
        IceCreamOrder instance = new IceCreamOrder("John Smith", date1, flavlist);
    }

}
