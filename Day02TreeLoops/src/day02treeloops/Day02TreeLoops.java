package day02treeloops;

import java.util.*;

public class Day02TreeLoops {
    
    static Scanner input = new Scanner(System.in);
    
    // get tree height from the user
    public static int userInput() {
        int height = 0;
        try {
            System.out.print("How big does your tree needs to be? ");
            height = input.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Error: Height should be interger. " + ex.getMessage());
        }
        return height;
    }
    
    // print tree pattern
    public static void printTree(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = height - i; j > 0; j--) {
                System.out.print("  ");
            }
            for (int j = 0; j < i; j++) {
                System.out.print("* ");
            }
            for (int j = 0; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println("");
        }        
    }

    public static void main(String[] args) {
        int height = userInput();
        printTree(height);
        
    }
    
}
