/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalflights;

import finalflights.Flight.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class Database {

    ArrayList<Flight> flightList = new ArrayList<>();
    private static String dbUrl = "jdbc:mysql://localhost:3306/finalflights";
    private static String username = "root";
    private static String password = "Sdf93jka63>";

    private Connection conn = null; // make connection private

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbUrl, username, password);
    }

    public ArrayList<Flight> getAllFlights() throws SQLException, IllegalArgumentException {
        ArrayList<Flight> list = new ArrayList<>();
        String sql = "SELECT id, onDate, fromCode, toCode, type, passengers FROM flights";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a godd practice to use try-with-resources for ResultSet so it is frred up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                Date onDate = result.getDate("onDate");
                String fromCode = result.getString("fromCode");
                String toCode = result.getString("toCode");
                String str = result.getString("type");
                Type type = Type.valueOf(str); // ex IllegalArgumentException
                int passengers = result.getInt("passengers");
                list.add(new Flight(id, onDate, fromCode, toCode, type, passengers));
            }
        }
        return list;
    }

    // fetch one record 
    public Flight getFlightById(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM flights WHERE id=?");
        statement.setInt(1, id);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                Date onDate = result.getDate("onDate");
                String fromCode = result.getString("fromCode");
                String toCode = result.getString("toCode");
                String str = result.getString("type");
                Type type = Type.valueOf(str); // ex IllegalArgumentException
                int passengers = result.getInt("passengers");
                return new Flight(id, onDate, fromCode, toCode, type, passengers);
            } else {
                throw new SQLException("Resord not found");
            }
        }
    }

    public void addFlight(Flight flight) throws SQLException {
        String sql = "INSERT INTO flights VALUES (NULL,?, ?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        java.sql.Date sqlDate = new java.sql.Date(flight.onDate.getTime());
        statement.setDate(1, sqlDate);
        statement.setString(2, flight.fromCode);
        statement.setString(3, flight.toCode);
        statement.setString(4, flight.type.toString());
        statement.setInt(5, flight.passengers);
        statement.executeUpdate();
        System.out.println("Record inserted");
    }

       public void updateFlight(Flight flight) throws SQLException {
        String sql = "UPDATE flights SET onDate=?, fromCode=?, toCode=?, type=?, passengers=? WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        java.sql.Date sqlDate = new java.sql.Date(flight.onDate.getTime());
        statement.setDate(1, sqlDate);
        statement.setString(2, flight.fromCode);
        statement.setString(3, flight.toCode);
        statement.setString(4, flight.type.toString());
        statement.setInt(5, flight.passengers);
        statement.setInt(6, flight.id);
        statement.executeUpdate();
        System.out.println("Record updated");
    }

    public void deleteFlight(Flight flight) throws SQLException {
        String sql = "DELETE FROM flights WHERE id=? ";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, flight.id);
        statement.executeUpdate();
        System.out.println("Record deleted id =" + flight.id);
    }

}
