/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalflights;

import java.util.Date;

public class Flight {

    int id;
    Date onDate;  // no custom date format, used the standard date format - lack of time
    String fromCode;
    String toCode;
    Type type;
    int passengers;

    enum Type {
        DOMESTIC, INTERNATIONAL, PRIVATE
    };

    public Flight(int id, Date onDate, String fromCode, String toCode,Type type, int passengers) {
        this.id = id;
        this.onDate = onDate;
        this.fromCode = fromCode;
        this.toCode = toCode;
        this.type = type;
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        return "Flight " +  id + ", Date:" + onDate + ", From: " + fromCode + ", To: " + toCode + ", Type: " + type + ", passengers: " + passengers;
    }

}
