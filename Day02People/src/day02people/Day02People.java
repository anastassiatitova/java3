package day02people;

import java.util.*;
import java.io.*;
import java.security.InvalidParameterException;

public class Day02People {

    static ArrayList<Person> people = new ArrayList<>();
    static final String FILE_NAME = "people.txt";

    public static void readFromFile() {
        try {
            try (Scanner fileInput = new Scanner(new File(FILE_NAME))) {
                while (fileInput.hasNextLine()) {
                    String str = fileInput.nextLine();
                    if (str.contains(";")) {
                        try {
                            String[] temp = str.split(";", 2);
                            int tempAge = Integer.parseInt(temp[1]);
                            people.add(new Person(temp[0], tempAge));
                        } catch (NumberFormatException ex) {
                            System.out.println("Error: Wrong age data. " + ex.getMessage());
                        } catch (InvalidDataException ex) {
                            System.out.println("Error: Invalid input. " + ex.getMessage());
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file. " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
//        Person p0 = new Person("Mike", 23);
//        p0.setName("John Doe");
//        p0.setAge(78);
//        System.out.println("Name is " + p0.getName());
        readFromFile();
        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.getName(), p.getAge());
        }
     }

}
