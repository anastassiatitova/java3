package day02people;

import java.security.InvalidParameterException;

public class Person {

    private String name;
    private int age;

// default Constructor    
//    public Person(){
//        super();
//    }
    public Person(String name, int age) throws InvalidDataException {
       setName(name);
       setAge(age);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age)throws InvalidDataException {
        if (age < 1 || age > 150) {
            throw new InvalidDataException("Age should be between 1 and 150");
        }
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidDataException{
        if (name.length() < 2 || name.length() > 20 || name.contains(";")) {
            throw new InvalidDataException("Name must be 2-20 characters long");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }

}
