/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07notepad;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author garde
 */
public class Day07Notepad extends javax.swing.JFrame implements ActionListener, PropertyChangeListener {

    // Variables 
    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
    private String openedFile = "";
    private JFrame frame;
    private boolean isModified = true; //check if text was modified

    /**
     * Creates new form Day07Notepad
     */
    public Day07Notepad() {
        initComponents();

        // setup FileChooser - Directory path and txt files only
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(filter);
//        fileChooser.setCurrentDirectory(new File("C:\\Users\\garde\\Documents\\JAVA\\java3\\Day07Notepad")); // not to do it

        // attache ActionListner to all buttons
        miFileNew.addActionListener(this);
        miFileOpen.addActionListener(this);
        miFileSave.addActionListener(this);
        miFileSaveAs.addActionListener(this);
        miFileExit.addActionListener(this);

        // attache PropertyChangeListner to text area
        taDocument.addPropertyChangeListener(this);

    }

    // action choice after button was clicked
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == miFileNew) { // ex UnsupportedOperationException
                openNewFile();
            } else if (e.getSource() == miFileOpen) {// ex UnsupportedOperationException
                openFile();
            } else if (e.getSource() == miFileSave) {// ex UnsupportedOperationException
                writeToFile();
            } else if (e.getSource() == miFileSaveAs) {// ex UnsupportedOperationException
                saveToFileAs();
            } else if (e.getSource() == miFileExit) {// ex UnsupportedOperationException
                if (isModified) {
                    exit();
                } else {
                    dispose();
                }
            }
        } catch (UnsupportedOperationException ex) {
            lblStatus.setText(ex.getMessage());
        }
    }

    // check is text field was changed
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        try {
            if (evt.getSource() == taDocument) {// ex UnsupportedOperationException
                isModified = true;
            }
        } catch (UnsupportedOperationException ex) {
            lblStatus.setText(ex.getMessage());
        }

    }

    // open new file
    public void openNewFile() {
        if (openedFile.isBlank()) { // no  file is opened
            taDocument.setText(" ");
            lblStatus.setText("New File");
        } else {                            // there is an opened file
            int n = JOptionPane.showConfirmDialog(
                    frame,
                    "Would you like to save unsaved changes to " + openedFile + "?",
                    "Notepad",
                    JOptionPane.YES_NO_CANCEL_OPTION);
            if (n == JOptionPane.YES_OPTION) {  // save opened file and open a new file
                writeToFile();
                taDocument.setText(" ");
                lblStatus.setText("New File");
            } else if (n == JOptionPane.NO_OPTION) { // open new file without saving
                taDocument.setText(" ");
                lblStatus.setText("New File");
            } else if (n == JOptionPane.CANCEL_OPTION) { // cancel to open new file
                lblStatus.setText("Open new file is cancelled by user");
            }
        }
    }

    // open file selectede by user
    public void openFile() {
        try {
            int returnVal = fileChooser.showOpenDialog(this); // display FileChooser
            if (returnVal == JFileChooser.APPROVE_OPTION) { // file is selected by user
                File file = fileChooser.getSelectedFile();  // get selected file
                openedFile = file.getName(); // save file name
                inputFromFile(); // input form file
                lblStatus.setText("Open file: " + openedFile); // display status
            } else { // action cancelled
                lblStatus.setText("Open command is cancelled by user");
            }
        } catch (HeadlessException ex) {
            lblStatus.setText(ex.getMessage());
        }
    }

    public void saveToFileAs() {
        try {
            int returnVal = fileChooser.showDialog(this, "Save As");//display FileChooser    ex  HeadlessException
            if (returnVal == JFileChooser.APPROVE_OPTION) { // file is selected by user
                File file = fileChooser.getSelectedFile(); //get selected file
                openedFile = file.getName(); // save file name
                writeToFile(); // write to file
                lblStatus.setText("Saved to file: " + openedFile); // display status
            } else {
                lblStatus.setText("Save command is cancelled by user");
            }
        } catch (HeadlessException ex) {
            lblStatus.setText(ex.getMessage());
        }
    }

    public void exit() {
        //default icon, custom title
        int n = JOptionPane.showConfirmDialog(
                frame,
                "Would you like to save unsaved changes to " + openedFile + "?",
                "Notepad",
                JOptionPane.YES_NO_CANCEL_OPTION);
        if (n == JOptionPane.YES_OPTION) {
            writeToFile();
            dispose();
        } else if (n == JOptionPane.NO_OPTION) {
            dispose();
        } else if (n == JOptionPane.CANCEL_OPTION) {
            lblStatus.setText("Exit is cancelled by user");
        }

    }

    //save to file
    public void writeToFile() {
        try (PrintWriter output = new PrintWriter(new File(openedFile))) { // ex IOException
            output.print(taDocument.getText());
            lblStatus.setText("Saved to file: " + openedFile);
            isModified = false;
        } catch (IOException ex) {
            lblStatus.setText("File not found: " + openedFile);
        }
    }

    // input from file to text field
    public void inputFromFile() {
        try (Scanner inputFile = new Scanner(new File(openedFile))) { // ex IOException
            String str = "";
            while (inputFile.hasNextLine()) {
                str += inputFile.nextLine() + "\n";
            }
            taDocument.setText(str);
        } catch (IOException ex) {
            lblStatus.setText("File not found: " + openedFile);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        lblStatus = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taDocument = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miFileNew = new javax.swing.JMenuItem();
        miFileOpen = new javax.swing.JMenuItem();
        miFileSave = new javax.swing.JMenuItem();
        miFileSaveAs = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miFileExit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(250, 250));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        lblStatus.setText("...");
        lblStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        getContentPane().add(lblStatus, java.awt.BorderLayout.PAGE_END);

        taDocument.setColumns(20);
        taDocument.setRows(5);
        jScrollPane1.setViewportView(taDocument);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        miFile.setText("File");
        miFile.setMinimumSize(new java.awt.Dimension(31, 250));

        miFileNew.setText("New");
        miFile.add(miFileNew);

        miFileOpen.setText("Open...");
        miFile.add(miFileOpen);

        miFileSave.setText("Save");
        miFile.add(miFileSave);

        miFileSaveAs.setText("Save As...");
        miFile.add(miFileSaveAs);
        miFile.add(jSeparator1);

        miFileExit.setText("Exit");
        miFile.add(miFileExit);

        jMenuBar1.add(miFile);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (isModified) {
            exit();
        } else {
            dispose();
        }

    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day07Notepad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day07Notepad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day07Notepad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day07Notepad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day07Notepad().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miFileExit;
    private javax.swing.JMenuItem miFileNew;
    private javax.swing.JMenuItem miFileOpen;
    private javax.swing.JMenuItem miFileSave;
    private javax.swing.JMenuItem miFileSaveAs;
    private javax.swing.JTextArea taDocument;
    // End of variables declaration//GEN-END:variables

}
