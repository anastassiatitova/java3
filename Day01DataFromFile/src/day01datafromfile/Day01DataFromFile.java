package day01datafromfile;

import java.util.*;
import java.io.*;

public class Day01DataFromFile {

    static ArrayList<String> nameList = new ArrayList<>();
    static ArrayList<Double> numList = new ArrayList<>();

    // write data to file
    static void writeToFile(String name) {
        try (PrintWriter output = new PrintWriter(new File("duplicates.txt"))) {
            output.print(name);
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        // Read data from file input.txt
        try {
            try (Scanner input = new Scanner(new File("input.txt"))) {
                String str = input.nextLine();
                while (input.hasNextLine()) {
                    try {
                        numList.add(Double.parseDouble(str));
                    } catch (NumberFormatException ex) {
                        nameList.add(str);
                    }
                    str = input.nextLine();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // Sort lists and display
        Collections.sort(nameList);
        Collections.sort(numList);
        System.out.print("Names sorted: ");
        System.out.println(nameList.toString().replace("[", "").replace("]", ""));
        System.out.print("Numbers sorted: ");
        System.out.println(numList.toString().replace("[", "").replace("]", ""));

        // Calcuate average of names
        double avgName = 0;
        for (String name : nameList) {
            avgName += name.length();
        }
        if (!nameList.isEmpty()) {
            avgName = avgName / nameList.size();
            System.out.printf("Average length of names: %.2f\n", avgName);
        }
        
        int count = 1;
        // Display duplicate names
        System.out.println("Duplicate elements from array using hash table");
        Map<String, Integer> nameAndCount = new HashMap<>();

        // build hash table with count 
        for (String name : nameList) {
            Integer n = nameAndCount.get(name);
            if (n == null) {
                nameAndCount.put(name, 1);
            } else {
                nameAndCount.put(name, ++count);
            }
        }
        // Disply duplicate names and write them to the file
        Set<Map.Entry<String, Integer>> entrySet = nameAndCount.entrySet();
        String nameRepeat = "";
        System.out.print("Duplicate element from array : ");
        for (Map.Entry<String, Integer> entry : entrySet) {
            if (entry.getValue() > 1) {
                System.out.printf("%s,", entry.getKey());
                nameRepeat += entry.getKey() + "\n";
            }
        }
        writeToFile(nameRepeat);
    }
}
